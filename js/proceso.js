
async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {

        alert(err);
    }
}

var titulo = "";

function consultarNotas(){
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";
    leerJSON(url).then(datos => { 
        titulo=datos.nombreMateria;
        buscarCodigo(datos.estudiantes);   
    })


}

function buscarCodigo(estudiantes) {
    let codigo = document.getElementById("codigo").value;
    for (x of estudiantes) { 
        if(x.codigo==codigo){
          drawTableEstudiantes(x);
           }else{
            return "No se encuentra un codigo igual";
        }
    }
  
}



function drawTableEstudiantes(x) {
    var data = new google.visualization.DataTable();
   // var data2 = new google.visualization.DataTable();
    //Encabezados de las tablas:
    data.addColumn('string', x.nombre);
   // crearEncabezados2(data2);
    //Crear las filas:
    data.addRows(2);
    data.setCell(0, 0, x.codigo + "");
    data.setCell(1, 0, titulo);
    
    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, { showRowNumber: false, width: '50%', height: '100%' });
}


